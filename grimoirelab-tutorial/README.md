# GrimoireLab Tutorial

### How to set it up locally

Clone this repository

```bash
$ git clone https://gitlab.summer-ospp.ac.cn/summer2021/210190824.git
```

Go to the created directory and make sure you have ruby 2.7.3 running.

```console
$ cd 210190824/grimoirelab-tutorial/
$ ruby -v
ruby 2.7.3p183 (2021-04-05 revision 6847ee089d) [x86_64-linux]
```

Then start serving the site locally:

```bash
$ bundle exec jekyll serve
```

Open the site in the browser of your choice: http://127.0.0.1:4000
