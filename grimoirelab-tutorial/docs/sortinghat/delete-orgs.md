---
layout: default
title: Remove Orgs
<<<<<<< Updated upstream
nav_order: 8
parent: Contributor Information Management
=======
nav_order: 3
parent: Affiliations in SortingHat
grand_parent: Managing/Editing Contributor Information
>>>>>>> Stashed changes
has_children: false
has_toc: false
---

# How to delete organisations from registry

To delete an organisation, simple look over to the `Organisations` table and click on the Bin icon to delete the organisation of your choice.

![delete-org](./assets/delete-org.png)
