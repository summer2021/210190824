---
layout: default
title: Contributors
nav_order: 1
parent: Understand personal engagement
grand_parent: Understanding Community Health
---

# Contributors
Question: Who are the contributors to a project?

A contributor is defined as anyone who contributes to the project in any way. This metric ensures that all types of contributions are fully recognized in the project.

### Objectives
Open source projects are comprised of a number of different contributors. Recognizing all contributors to a project is important in knowing who is helping with such activities as code development, event planning, and marketing efforts.

### Implementation
Collect author names from collaboration tools a project uses.

#### Aggregators:
- Count. Total number of contributors during a given time period.

#### Parameters:
- Period of time. Start and finish date of the period. Default: forever. Period during which contributions are counted.

#### Filters
By location of engagement. For example:

- Commit authors
- Issue authors
- Review participants, e.g., in pull requests
- Mailing list authors
- Event participants
- IRC authors
- Blog authors
- By release cycle
- Timeframe of activity in the project, e.g, find new contributors
- Programming languages of the project
- Role or function in project

#### Visualizations

1. List of contributor names (often with information about their level of engagement)

![Contributor names and info](../assets/contributors_top-contributor-info.png)

2. Summary number of contributors

![Summary number of contributors](../assets/contributors_summary-contributor-number.png)

3. Change in the number of active contributors over time

![Contributor growth](../assets/contributors_growth.png)

4. New contributors (sort list of contributors by date of first contribution)

![New contributors](../assets/contributors_first-commit-date.png)