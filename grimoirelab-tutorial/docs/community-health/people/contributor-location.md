---
layout: default
title: Contributor Location
nav_order: 2
parent: Understand personal engagement
grand_parent: Understanding Community Health
---

# Contributor Location
Question: What is the location of contributors?

Geographical location from which contributors contribute, where they live, or where they work.

### Objectives
To determine global locations of contributors in an effort to understand work practices and times zones. To identify where contributions do not come from in an effort to improve engagement in these areas.

### Implementation

#### Filters

Filter contributions by:

* **Location.** Attempt to group locations in regions to have multiple levels of reporting. Location is a purposely ambiguous term in this context, and could refer to region, country, state, locale, or time zone.
* **Period of time.** Start and finish date of the period. Default: forever. Period during which contributions are counted.
* **Type of contributor**, for example:
  * Repository authors
  * Issue authors
  * Code review participants
  * Mailing list authors
  * Event participants
  * IRC authors
  * Blog authors
  * By release cycle
  * Programming languages of the project
  * Role or function in project


#### Visualizations

Dot Density Map:

![Contributor Location Dot Density Map](../assets/contributor-location_dot-density-map.png)

Source: [https://chaoss.biterg.io/goto/a62f3584a41c1c4c1af5d04b9809a860](https://chaoss.biterg.io/goto/a62f3584a41c1c4c1af5d04b9809a860)

Visual heat map:

![Contributor Location Heatmap](../assets/contributor-location_heatmap.png)

Source:  [https://blog.bitergia.com/2018/11/20/ubers-community-software-development-analytics-for-open-source-offices](https://blog.bitergia.com/2018/11/20/ubers-community-software-development-analytics-for-open-source-offices)