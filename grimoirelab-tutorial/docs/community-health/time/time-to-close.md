---
layout: default
title: Time to Close
nav_order: 5
parent: Identify when contributions happen
grand_parent: Understanding Community Health
has_children: false
has_toc: false
---

# Time to close
Question: How much time passes between creating and closing an operation such as an issue, change request, or support ticket ?

### Description

The time to close is the total amount of time that passes between the creation and closing of an operation such as an issue, change request, or support ticket. The operation needs to have an open and closed state, as is often the case in code review processes, question and answer forums, and ticketing systems.

### Objectives
1. Determining how responsive a community is can help efforts to be inclusive, attract, and retain new and existing contributors.
2. Identifying characteristics of operations that impact an operation closing quickly or slowly (e.g., finding best practices, areas of improvement, assess efficiency).
3. Identifying bias for timely responses to different community members.
4. Detecting a change in community activity (e.g., to indicate potential maintainer burnout, reduction in the diversity of contributions)
5. Understand how the time to close an issue or change request is related to merge success or failure.

### Implementation
#### Filters
 - Creator of operation (e.g., new contributor vs. maintainer)
 - First closed, final closed
 - Labels (e.g., bug vs. new feature)
 - Change Request Merge Status (e.g. Time to Merge or Time to Close without Merge)

### Visualizations

![time to close](../assets/time-to-close_1.png)
