---
layout: default
title: Activity Dates and Times
nav_order: 1
parent: Identify when contributions happen
grand_parent: Understanding Community Health
has_children: false
has_toc: false
---

# Activity Dates and Time
Question: What are the dates and timestamps of when a contributor activities occur ?

Individuals engage in activities in open source projects at various times of the day. This metric is aimed at determining the dates and times of when individual activities were completed. The data can be used to probabilistically estimate where on earth contributions come from in cases where the time zone is not UTC.

### Objectives
- Improve transparency for employers about when organizational employees are engaging with open source projects
- Improve transparency for open source project and community managers as to when activity is occurring

### Implementation
#### Filters
 - Individual by Organization
 - Aggregation of time by UTC time
 - Can show what times across the globe contributions are made; when the project is most active.
 - Aggregation of time by local time
    - Can show what times of day in their local times they contribute. Conclusions about the If contributions are more during working hours, or if contributions are more during evening hours.
 - Repository ID
 - Segment of a community, (e.g., GrimoireLab has more EU time zones activity and Augur more US time zones activity)

### Visualizations

![activity dates and times 1](../assets/activity-dates-and-times_1.png)

![activity dates and times 2](../assets/activity-dates-and-times_2.png)

![activity dates and times 3](../assets/activity-dates-and-times_3.png)

![activity dates and times 4](../assets/activity-dates-and-times_4.png)