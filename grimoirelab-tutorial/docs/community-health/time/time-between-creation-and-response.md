---
layout: default
title: Time to First Response
nav_order: 4
parent: Identify when contributions happen
grand_parent: Understanding Community Health
has_children: false
has_toc: false
---

# Time to First Response
Question: How much time passes between when an activity requiring attention is created and the first response ?

The first response to an activity can sometimes be the most important response. The first response shows that a community is active and engages in conversations. A long time to respond to an activity can be a sign that a community is not responsive. A short time to respond to an activity can help to engage more members into further discussions and within the community.

### Objectives
Identify cadence of first response across a variety of activities, including PRs, Issues, emails, IRC posts, etc. Time to first response is an important consideration for new and long-time contributors to a project along with overall project health.

### Implementation
Time to first response of an activity = time first response was posted to the activity - time the activity was created.

#### Filters
 - Role of responder, e.g., only count maintainer responses
 - Automated responses, e.g., only count replies from real people by filtering bots and other automated replies
 - Type of Activity, e.g., issues (see metric Issue Response Time), emails, chat, change requests

### Visualizations

![time-to-first-response_efficiency-timing-overview](../assets/time-to-first-response_efficiency-timing-overview.png)

![time-to-first-response_augur-ttc-1](../assets/time-to-first-response_augur-ttc-1.png)

![time-to-first-response_augur-ttc-2](../assets/time-to-first-response_augur-ttc-2.png)