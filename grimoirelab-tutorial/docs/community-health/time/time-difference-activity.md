---
layout: default
title: Bustiness
nav_order: 2
parent: Identify when contributions happen
grand_parent: Understanding Community Health
has_children: false
has_toc: false
---

# Bustiness
Question: How are short timeframes of intense activity, followed by a corresponding return to a typical pattern of activity, observed in a project ?

There are a number of reasons that may prompt a sudden increase or decrease in the amount of activity within a repository. These increases and decreases appear both as a sudden change in activity against the average amount of activity. Burstiness is a way of understanding the cycle of activity in existing metrics, like issues, merge requests, mailing lists, commits, or comments. Examples of root causes for bursts in activity include:

- Release cycles
- Global pandemics
- Hackathon activities
- Mentorship programs
- Conferences, meetups, and other events where tools are presented
- Conventional and social media announcements and mentions
- Critical bugs as raising awareness and getting people’s attention
- Community design meetings or brainstorming meetings to address a particular issue
- Community members show up from another community that is relying on your project (e.g., dependencies)

### Objectives
- To identify impacts of root causes of a burst in activity
- To provide awareness when project activity unknowingly goes up
- To help capture the meaningfulness of increases or decreases in project activity
- To help the community and maintainers prepare for future bursts that follow a pattern
- To help measure the impact of influential external activities
- To differentiate skewed activity versus normal activity

### Implementation
#### Filters
- Stars
- Forks
- Issues or bug reports
- Labels
- Downloads
- Release Tags
- Change Requests
- Mail List Traffic
- Documentation additions or revisions
- New Repositories
- Feature Requests
- Messaging Conversations
- Conventional and Social Media Activity
- Conference Attendance and Submissions

### Visualization

![burstiness](../assets/grimoirelab_burstiness.png)